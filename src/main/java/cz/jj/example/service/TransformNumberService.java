package cz.jj.example.service;

import org.springframework.stereotype.Service;

@Service
public class TransformNumberService {

    public long transformNumber(long numberToTransform) {
        String[] numbers = String.valueOf(numberToTransform).split("");

        moveSmallNumbers(numbers);
        multiplyHighNumbers(numbers);
        removeLuckyNumbers(numbers);

        StringBuilder numberBuilder = new StringBuilder();
        int countOfEven = buildNumberAndCountEven(numbers, numberBuilder);
        long result = Long.parseLong(numberBuilder.toString());

        return result / countOfEven;
    }

    int buildNumberAndCountEven(String[] numbers, StringBuilder numberBuilder) {

        int count = 0;
        for (String number : numbers) {
            try {
                if (getNumber(number) % 2 == 0) {
                    count++;
                }
            } catch (NumberFormatException exception) {
//                DO nothing - empty character (instead of 7)
            }

            numberBuilder.append(number);
        }

        return count;
    }

    void removeLuckyNumbers(String[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            int number = getNumber(numbers[i]);
            if(number == 7) {
                numbers[i] = "";
            }
        }
    }

    void multiplyHighNumbers(String[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            int number = getNumber(numbers[i]);
            if(number >= 8) {
                numbers[i] = String.valueOf(number * 2);
            }
        }
    }

    void moveSmallNumbers(String[] numbers) {
        for (int i = numbers.length - 2; i >= 0; i--) {
            if(getNumber(numbers[i]) <= 3) {
                String smallNumber = numbers[i];
                numbers[i] = numbers[i+1];
                numbers[i+1] = smallNumber;
            }

        }
    }

    private int getNumber(String number) {
        return Integer.parseInt(number);
    }

}
