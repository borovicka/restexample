package cz.jj.example.controller;

import cz.jj.example.service.TransformNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transform")
public class TransformNumberController {

    @Autowired
    private TransformNumberService transformNumberService;

    @GetMapping("/{numberToTransform}")
    public ResponseEntity<Long> transformNumber(@PathVariable("numberToTransform") long numberToTransform) {
        long transformedNumber = transformNumberService.transformNumber(numberToTransform);
        return new ResponseEntity<>(transformedNumber, HttpStatus.OK);
    }
}
