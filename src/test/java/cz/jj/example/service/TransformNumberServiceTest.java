package cz.jj.example.service;

import cz.jj.example.AppConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class TransformNumberServiceTest {
    @Autowired
    private TransformNumberService transformNumberService;


    @Test
    public void buildNumberAndCountEven() {
        StringBuilder sb = new StringBuilder();
        int countEven = transformNumberService.buildNumberAndCountEven("45326181".split(""), sb);

        assertEquals(4, countEven);
        assertEquals("45326181", sb.toString());
    }

    @Test
    public void removeLuckyNumbers() {
        String[] numbers = buildInput("453267181");
        transformNumberService.removeLuckyNumbers(numbers);

        assertEquals("45326181", buildResult(numbers));
    }

    @Test
    public void multiplyHighNumbers() {
        String[] numbers = buildInput("45326791");
        transformNumberService.multiplyHighNumbers(numbers);

        assertEquals("453267181", buildResult(numbers));
    }

    @Test
    public void moveSmallNumbers() {
        String[] numbers = buildInput("43256791");
        transformNumberService.moveSmallNumbers(numbers);

        assertEquals("45326791", buildResult(numbers));
    }

    private String buildResult(String[] numbers) {
        StringBuilder sb = new StringBuilder();
        for (String number : numbers) {
            sb.append(number);
        }

        return sb.toString();
    }

    private String[] buildInput(String input) {
        return input.split("");
    }

}
